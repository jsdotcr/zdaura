<a name="0.2.1"></a>
## [0.2.1](https://gitlab.com/jsdotcr/zdaura/compare/0.2.0...0.2.1) (2017-11-28)


### Bug Fixes

* **service-gitlab:** allow empty arrays of labels to be added/removed ([8ca5e5a](https://gitlab.com/jsdotcr/zdaura/commit/8ca5e5a))
* **start:** wrong gitlab configstore prefix ([c998e9d](https://gitlab.com/jsdotcr/zdaura/commit/c998e9d))



<a name="0.2.0"></a>
# [0.2.0](https://gitlab.com/jsdotcr/zdaura/compare/0.1.0...0.2.0) (2017-11-27)


### Bug Fixes

* define shared status constants ([83d7dde](https://gitlab.com/jsdotcr/zdaura/commit/83d7dde))
* **service-gitlab:** update and fix assertions, fix typos ([83605c8](https://gitlab.com/jsdotcr/zdaura/commit/83605c8))


### Features

* **review:** implement review task ([1bee42a](https://gitlab.com/jsdotcr/zdaura/commit/1bee42a))
* **service-gitlab:** configure labels for each status ([83510c2](https://gitlab.com/jsdotcr/zdaura/commit/83510c2))
* **service-trello:** setup trello and configure statuses ([07a49fd](https://gitlab.com/jsdotcr/zdaura/commit/07a49fd))
* **start:** add start main task ([e86d376](https://gitlab.com/jsdotcr/zdaura/commit/e86d376))
* **track:** implement track command ([8d8b6db](https://gitlab.com/jsdotcr/zdaura/commit/8d8b6db))



<a name="0.1.0"></a>
# [0.1.0](https://gitlab.com/jsdotcr/zdaura/compare/f1bb99a...0.1.0) (2017-11-25)


### Bug Fixes

* **service-todoist:** simplify setup ([f1bb99a](https://gitlab.com/jsdotcr/zdaura/commit/f1bb99a))


### Features

* **estimate:** add estimation task ([5a70220](https://gitlab.com/jsdotcr/zdaura/commit/5a70220))
* **logging:** add basic logging helper functions ([f06394f](https://gitlab.com/jsdotcr/zdaura/commit/f06394f))
* **service-gitlab:** add gitlab connection ([ad30746](https://gitlab.com/jsdotcr/zdaura/commit/ad30746)), closes [#1](https://gitlab.com/jsdotcr/zdaura/issues/1) [#2](https://gitlab.com/jsdotcr/zdaura/issues/2)



