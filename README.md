# Zdaura
My own open-sourced, possibly always-evolving, dev workflow.

Progresses tracked on the Gitlab [issue board](https://gitlab.com/jsdotcr/zdaura/boards) and/or on the [M1: basics](https://gitlab.com/jsdotcr/zdaura/milestones/1) milestone.


# Services currently in use
* Gitlab
* Trello
* Todoist (via PomoDoneApp)


# Badges
Because we all love badges!

[![Known Vulnerabilities](https://snyk.io/test/npm/zdaura/badge.svg)](https://snyk.io/test/npm/zdaura)
[![FOSSA Status](https://app.fossa.io/api/projects/git%2Bgitlab.com%2Fjsdotcr%2Fzdaura.svg?type=small)](https://app.fossa.io/projects/git%2Bgitlab.com%2Fjsdotcr%2Fzdaura?ref=badge_small)