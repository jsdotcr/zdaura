const debug = require('debug')('zdaura:trello:fetch-lists')
const fetch = require('isomorphic-fetch')
const { URL } = require('url')

const { getConfiguration } = require('./config')

function fetchLists () {
  const { apiKey, board, token } = getConfiguration()
  debug('configuration fetched')
  const url = new URL(`https://api.trello.com/1/boards/${board}/lists`)
  debug('url build', url)

  url.searchParams.append('key', apiKey)
  url.searchParams.append('token', token)
  debug('url to fetch', url.toString())

  return fetch(
    url.toString(),
    {
      method: 'GET'
    }
  )
  .then(response => response.json())
}

exports.fetchLists = fetchLists
