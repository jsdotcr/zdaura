const debug = require('debug')('zdaura:gitlab:config')
const joi = require('joi')

const defaultConfiguration = require('../../config')

function getConfiguration (configuration = defaultConfiguration) {
  debug('will fetch configuration')
  joi.assert(configuration, joi.object().keys({
    get: joi.func().required()
  }).unknown(true))
  debug('configuration ok')

  return joi.attempt({
    apiKey: configuration.get('trello.apiKey'),
    board: configuration.get('trello.board'),
    token: configuration.get('trello.token')
  }, joi.object().keys({
    apiKey: joi.string().length(32).required(),
    board: joi.string().length(8).required(),
    token: joi.string().length(64).required()
  }))
}

exports.getConfiguration = getConfiguration
