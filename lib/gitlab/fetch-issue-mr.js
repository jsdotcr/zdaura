const debug = require('debug')('zdaura:gitlab:fetch-issue-or-mr')
const fetch = require('isomorphic-fetch')
const joi = require('joi')
const { URL } = require('url')

const { getConfiguration, ISSUE, MERGE_REQUEST } = require('./config')

function updateIssueOrMr (args) {
  const { project, token } = getConfiguration()
  debug('configuration fetched')
  const { duration, iid, labels, subResource, title, type } = joi.attempt(args, joi.object().keys({
    duration: joi.string().optional(),
    iid: joi.number().required(),
    labels: joi.array().optional(),
    subResource: joi.string().allow('time_estimate', 'add_spent_time').optional(),
    title: joi.string().optional(),
    type: joi.string().valid(ISSUE, MERGE_REQUEST).required()
  }).unknown(true))
  debug('validations passed', args, subResource)
  const url = new URL(`https://gitlab.com/api/v4/projects/${project}/${type}/${iid}`)
  debug('url build', url)

  if (subResource) {
    debug('will append subresource to the url', subResource)
    url.pathname += `/${subResource}`
  }
  if (duration) {
    url.searchParams.append('duration', duration)
  }
  if (labels) {
    url.searchParams.append('labels', labels)
  }
  if (title) {
    url.searchParams.append('title', title)
  }
  debug('url to fetch', url.toString())

  return fetch(
    url.toString(),
    {
      method: subResource ? 'POST' : 'PUT',
      headers: {
        'PRIVATE-TOKEN': token
      }
    }
  )
  .then(response => response.json())
}

exports.updateIssueOrMr = updateIssueOrMr
