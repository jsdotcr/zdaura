const debug = require('debug')('zdaura:gitlab:issue')
const fetch = require('isomorphic-fetch')
const { URL } = require('url')

const { getConfiguration } = require('./config')

function getLabels () {
  const { project, token } = getConfiguration()
  const url = new URL(`https://gitlab.com/api/v4/projects/${project}/labels`)
  debug('getLabels', 'URL built', url)

  return fetch(
    url.toString(),
    {
      method: 'GET',
      headers: {
        'PRIVATE-TOKEN': token
      }
    }
  )
  .then(response => response.json())
  .then(response =>
    response.map(({ description, id, name }) => ({
      description,
      id,
      name
    })))
}

exports.getLabels = getLabels
