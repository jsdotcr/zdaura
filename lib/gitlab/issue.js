const debug = require('debug')('zdaura:gitlab:issue')
const fetch = require('isomorphic-fetch')
const joi = require('joi')
const { URL } = require('url')

const { getConfiguration, ISSUE } = require('./config')
const { updateIssueOrMr } = require('./fetch-issue-mr')

function getIssue (args) {
  const { project, token } = getConfiguration()
  const { issueId, subResource } = joi.attempt(args, joi.object().keys({
    issueId: joi.number().required(),
    subResource: joi.string().allow('closed_by').optional()
  }))
  const url = new URL(`https://gitlab.com/api/v4/projects/${project}/issues/${issueId}`)
  debug('getIssue', 'validation passed, URL build', args, url)

  if (subResource) {
    debug('subResource', subResource)
    url.pathname += `/${subResource}`
  }

  return fetch(
    url.toString(),
    {
      method: 'GET',
      headers: {
        'PRIVATE-TOKEN': token
      }
    }
  )
  .then(response => response.json())
  .then(response => {
    if (Array.isArray(response)) {
      return response
    }
    return Object.assign({}, response, {
      issueId
    })
  })
}

function updateIssue (args) {
  const options = joi.attempt(args, joi.object().keys({
    duration: joi.string().optional(),
    issueId: joi.number().required(),
    labels: joi.array().optional(),
    subResource: joi.string().allow('time_estimate', 'add_spent_time').optional(),
    title: joi.string().optional()
  }).options({
    stripUnknown: true
  }))
  debug('updateIssue', 'validation passed', options)

  return updateIssueOrMr(Object.assign({}, options, {
    iid: options.issueId,
    type: ISSUE
  }))
}

function addEstimate (args) {
  joi.assert(args, joi.object().keys({
    duration: joi.string().regex(/^([0-9]+(d|h|m))+$/gm),
    issueId: joi.number().required()
  }))
  debug('addEstimate', 'validation passed', args)

  return updateIssue(Object.assign({}, args, {
    subResource: 'time_estimate'
  }))
}

function trackTime (args) {
  joi.assert(args, joi.object().keys({
    duration: joi.string().regex(/^([0-9]+(d|h|m))+$/gm),
    issueId: joi.number().required()
  }))
  debug('trackTime', 'validation passed', args)

  return updateIssue(Object.assign({}, args, {
    subResource: 'add_spent_time'
  }))
}

exports.addEstimate = addEstimate
exports.getIssue = getIssue
exports.updateIssue = updateIssue
exports.trackTime = trackTime
