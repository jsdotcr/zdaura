const debug = require('debug')('zdaura:gitlab:mr')
const joi = require('joi')

const { MERGE_REQUEST } = require('./config')
const { updateIssueOrMr } = require('./fetch-issue-mr')
const { getIssue } = require('./issue')

function getMergeRequest (args) {
  const { issueId } = joi.attempt(args, joi.object().keys({
    issueId: joi.number().required()
  }))

  return getIssue({
    issueId: issueId,
    subResource: 'closed_by'
  })
    .then(mergeRequests => {
      debug('post getIssue', mergeRequests.length)
      joi.assert(mergeRequests, joi.array().min(1))
      return mergeRequests[0]
    })
    .then(mergeRequest => Object.assign({}, mergeRequest, {
      mergeRequestId: mergeRequest.iid
    }))
}

function updateMergeRequest (args) {
  const options = joi.attempt(args, joi.object().keys({
    mergeRequestId: joi.number().required(),
    labels: joi.array().optional(),
    title: joi.string().optional()
  }).options({
    stripUnknown: true
  }))
  debug('updateMergeRequest', 'validation passed', options)

  return updateIssueOrMr(Object.assign({}, options, {
    iid: options.mergeRequestId,
    type: MERGE_REQUEST
  }))
}

exports.getMergeRequest = getMergeRequest
exports.updateMergeRequest = updateMergeRequest
