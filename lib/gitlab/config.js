const debug = require('debug')('zdaura:gitlab:config')
const joi = require('joi')

const defaultConfiguration = require('../../config')

function getConfiguration (configuration = defaultConfiguration) {
  debug('will fetch configuration')
  joi.assert(configuration, joi.object().keys({
    get: joi.func().required()
  }).unknown(true))
  debug('configuration ok')

  return joi.attempt({
    project: configuration.get('gitlab.project'),
    token: configuration.get('gitlab.token')
  }, joi.object().keys({
    project: joi.string().required(),
    token: joi.string().length(20)
  }))
}

exports.getConfiguration = getConfiguration
exports.ISSUE = 'issues'
exports.MERGE_REQUEST = 'merge_requests'
