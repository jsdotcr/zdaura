const debug = require('debug')('zdaura:gitlab:update')
const joi = require('joi')
const uniq = require('lodash.uniq')

exports.addLabels = function addLabels (payload, labelsToAdd = []) {
  debug('addLabels')
  joi.assert(payload.labels, joi.array().required(), 'Labels from the payload')
  joi.assert(labelsToAdd, joi.array().required(), 'Labels to add')
  debug('addLabels', payload.labels, labelsToAdd)
  return Object.assign({}, payload, {
    labels: uniq(payload.labels.slice().concat(labelsToAdd))
  })
}

exports.removeLabels = function removeLabels (payload, labelsToRemove = []) {
  debug('removeLabels')
  joi.assert(payload.labels, joi.array().required(), 'Labels from the payload')
  joi.assert(labelsToRemove, joi.array().required(), 'Labels to remove')
  debug('removeLabels', payload.labels, labelsToRemove)
  return Object.assign({}, payload, {
    labels: payload.labels.filter(label => !labelsToRemove.includes(label))
  })
}

exports.addWIP = function addWIP (payload) {
  joi.assert(payload.title, joi.string().required(), 'Title from the payload')
  debug('addWIP', payload.title)
  return Object.assign({}, payload, {
    title: `WIP: ${payload.title}`
  })
}

exports.removeWIP = function removeWIP (payload) {
  joi.assert(payload.title, joi.string(), 'Title from the payload')
  debug('removeWIP', payload.title)
  return Object.assign({}, payload, {
    title: payload.title.replace('WIP: ', '')
  })
}
