const { blue, bold, red, gray } = require('chalk')

function ok (message) {
  console.info(bold(gray('OK!')), message)
}

function ko (error) {
  if (error instanceof Error) {
    console.error(bold(red('KO!')), error.message)
    throw error
  }
  console.error(bold(red('KO!')), error)
  throw new Error(error)
}

function info (message) {
  console.info(blue('INFO'), message)
}

exports.ok = ok
exports.ko = ko
exports.info = info
