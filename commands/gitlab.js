exports.command = 'gitlab <command>'
exports.desc = 'Gitlab connection'
exports.builder = function (yargs) {
  return yargs.commandDir('gitlab')
}
