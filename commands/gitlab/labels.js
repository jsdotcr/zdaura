const { magenta } = require('chalk')
const debug = require('debug')('zdaura:gitlab:labels')
const inquirer = require('inquirer')

const { ok } = require('../../lib/log')
const configuration = require('../../config')
const { TODO, DOING, REVIEW, DONE } = require('../../config/statuses')
const { getLabels } = require('../../lib/gitlab/labels')

exports.command = 'labels'
exports.desc = 'Retrieve gitlab labels for the current project'
exports.handler = async function (args) {
  return getLabels()
    .then(response =>
      response
        .map(({ description, name }) => ({
          value: name,
          name: `${name} (${description})`
        }))
    )
    .then(choices =>
      inquirer.prompt([
        {
          type: 'checkbox',
          name: TODO,
          message: `Which are the labels for the ${magenta('To Do')} status?`,
          choices
        },
        {
          type: 'checkbox',
          name: DOING,
          message: `Which are the labels for the ${magenta('Doing')} status?`,
          choices
        },
        {
          type: 'checkbox',
          name: REVIEW,
          message: `Which are the labels for the ${magenta('In Review')} status?`,
          choices
        },
        {
          type: 'checkbox',
          name: DONE,
          message: `Which are the labels for the ${magenta('Done')} status?`,
          choices
        }
      ])
    )
    .then(args => {
      Object.entries(args)
        .forEach(([ key, id ]) =>
          configuration.set(`gitlab.status.${key}`, id)
        )
      debug(args)
      ok('Gitlab labels set up')
    })
}
