const joi = require('joi')

const { ok } = require('../../lib/log')
const configuration = require('../../config')

exports.command = 'set <what> <value>'
exports.desc = 'Connect to gitlab'
exports.handler = function (args) {
  const { what, value } = joi.attempt(args, joi.object().keys({
    value: joi.string().required(),
    what: joi.string().valid('token', 'project')
  }).unknown(true))

  configuration.set(`gitlab.${what}`, value)

  return ok(`gitlab.${what} configuration option set`)
}
