const debug = require('debug')('zdaura:todoist:connect')
const joi = require('joi')

const configuration = require('../../config')

exports.command = 'connect [token]'
exports.desc = 'Connect to Todoist'
exports.handler = function (args) {
  const { token } = joi.attempt(args, joi.object().keys({
    token: joi.string().length(20).required()
  }).unknown(true))
  debug('validation passed', args)

  configuration.set('todoist.token', token)
}
