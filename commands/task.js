exports.command = 'task <command>'
exports.desc = 'Task handlers'
exports.builder = function (yargs) {
  return yargs.commandDir('task')
}
