const { bold, magenta, bgBlue, white } = require('chalk')

exports.command = 'trello <command>'
exports.desc = `${bgBlue(white('Trello connector'))}
Plese remember that you will need to set up the parameters ${bold(magenta('apiKey'))},
${bold(magenta('token'))} and ${bold(magenta('board'))} to make this connector work.

The ${bold(magenta('apiKey'))} can be retrieved from https://trello.com/app-key
From the very same page, follow the link "generate a token" to get the
${bold(magenta('token'))} as well.`
exports.builder = function (yargs) {
  return yargs.commandDir('trello')
}
