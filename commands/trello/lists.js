const { magenta } = require('chalk')
const inquirer = require('inquirer')

const { ok } = require('../../lib/log')
const configuration = require('../../config')
const { TODO, DOING, REVIEW, DONE } = require('../../config/statuses')
const { fetchLists } = require('../../lib/trello/fetch-lists')

exports.command = 'lists'
exports.desc = 'Retrieve trello lists for the current board'
exports.handler = async function (args) {
  return fetchLists()
    .then(response =>
      response
        .filter(list => !list.closed)
        .map(({ id, name }) => ({
          value: id,
          name
        }))
    )
    .then(choices =>
      inquirer.prompt([
        {
          type: 'list',
          name: TODO,
          message: `Which is the ${magenta('To Do')} list?`,
          choices
        },
        {
          type: 'list',
          name: DOING,
          message: `Which is the ${magenta('Doing')} list?`,
          choices
        },
        {
          type: 'list',
          name: REVIEW,
          message: `Which is the ${magenta('In Review')} list?`,
          choices
        },
        {
          type: 'list',
          name: DONE,
          message: `Which is the ${magenta('Done')} list?`,
          choices
        }
      ])
    )
    .then(args => {
      Object.entries(args)
        .forEach(([ key, id ]) =>
          configuration.set(`trello.status.${key}`, id)
        )
      ok('Trello lists set up')
    })
}
