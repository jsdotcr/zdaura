const joi = require('joi')

const { ok } = require('../../lib/log')
const configuration = require('../../config')

exports.command = 'set <what> <value>'
exports.desc = 'Set trello configuration options'
exports.handler = function (args) {
  const { what, value } = joi.attempt(args, joi.object().keys({
    value: joi.string().required(),
    what: joi.string().valid('apiKey', 'board', 'token')
  }).unknown(true))

  configuration.set(`trello.${what}`, value)

  return ok(`trello.${what} configuration option set`)
}
