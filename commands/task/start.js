const joi = require('joi')

const configuration = require('../../config')
const { TODO, DOING } = require('../../config/statuses')
const { getIssue, updateIssue } = require('../../lib/gitlab/issue')
const { getMergeRequest, updateMergeRequest } = require('../../lib/gitlab/merge-request')
const { addLabels, addWIP, removeLabels } = require('../../lib/gitlab/update')
const { info, ok, ko } = require('../../lib/log')

exports.command = 'start <issueId>'
exports.desc = 'Start working on an issue'
exports.handler = async function (args) {
  const { issueId } = joi.attempt(args, joi.object().keys({
    issueId: joi.number().required()
  }).unknown(true))
  info('Will update issue and MR for you now')

  return Promise.all([
    getIssue({ issueId })
      .then(response => removeLabels(response, configuration.get(`gitlab.status.${TODO}`)))
      .then(response => addLabels(response, configuration.get(`gitlab.status.${DOING}`)))
      .then(updateIssue),
    getMergeRequest({ issueId })
    .then(response => removeLabels(response, configuration.get(`gitlab.status.${TODO}`)))
    .then(response => addLabels(response, configuration.get(`gitlab.status.${DOING}`)))
    .then(addWIP)
    .then(updateMergeRequest)
  ])
    .then(() => {
      ok(`Issue and merge requests updated!`)
    })
    .catch(ko)
}
