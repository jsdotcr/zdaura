const joi = require('joi')

const { addEstimate } = require('../../lib/gitlab/issue')
const { info, ok, ko } = require('../../lib/log')

exports.command = 'estimate <issueId> <duration>'
exports.desc = 'Estimate time for an issue'
exports.handler = async function (args) {
  const { duration, issueId } = joi.attempt(args, joi.object().keys({
    duration: joi.string().regex(/^([0-9]+(d|h|m))+$/gm),
    issueId: joi.number().required()
  }).unknown(true))
  info('Will update estimations for you now')

  return addEstimate({
    duration,
    issueId
  })
  .then(({
    human_total_time_spent: totalTimeSpent,
    human_time_estimate: timeEstimate
  }) => {
    ok(`${timeEstimate} is the new estimation for issue #${issueId}`)
  })
  .catch(ko)
}
