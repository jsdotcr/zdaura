const joi = require('joi')

const configuration = require('../../config')
const { DOING, REVIEW } = require('../../config/statuses')
const { getIssue, updateIssue } = require('../../lib/gitlab/issue')
const { getMergeRequest, updateMergeRequest } = require('../../lib/gitlab/merge-request')
const { addLabels, removeLabels, removeWIP } = require('../../lib/gitlab/update')
const { info, ok, ko } = require('../../lib/log')

exports.command = 'review <issueId>'
exports.desc = 'Move working issue to the Review phase'
exports.handler = async function (args) {
  const { issueId } = joi.attempt(args, joi.object().keys({
    issueId: joi.number().required()
  }).unknown(true))
  info('Will update issue and MR for you now')

  return Promise.all([
    getIssue({ issueId })
      .then(response => removeLabels(response, configuration.get(`gitlab.status.${DOING}`)))
      .then(response => addLabels(response, configuration.get(`gitlab.status.${REVIEW}`)))
      .then(updateIssue),
    getMergeRequest({ issueId })
    .then(response => removeLabels(response, configuration.get(`gitlab.status.${DOING}`)))
    .then(response => addLabels(response, configuration.get(`gitlab.status.${REVIEW}`)))
    .then(removeWIP)
    .then(updateMergeRequest)
  ])
    .then(() => {
      ok(`Issue and merge requests updated!`)
    })
    .catch(ko)
}
