const joi = require('joi')

const { trackTime } = require('../../lib/gitlab/issue')
const { info, ok, ko } = require('../../lib/log')

exports.command = 'track <issueId> <duration>'
exports.desc = 'Track time on an issue'
exports.handler = async function (args) {
  const { duration, issueId } = joi.attempt(args, joi.object().keys({
    duration: joi.string().regex(/^([0-9]+(d|h|m))+$/gm),
    issueId: joi.number().required()
  }).unknown(true))
  info('Will track last progresses on the issue for you now')

  return trackTime({
    duration,
    issueId
  })
  .then(({
    human_total_time_spent: totalTimeSpent,
    human_time_estimate: timeEstimate
  }) => {
    ok(`${totalTimeSpent} has been spent on this task so far. Time estimate is ${timeEstimate}`)
  })
  .catch(ko)
}
