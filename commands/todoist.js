exports.command = 'todoist <command>'
exports.desc = 'Todoist connection'
exports.builder = function (yargs) {
  return yargs.commandDir('todoist')
}
